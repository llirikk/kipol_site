import React from "react";
import Slider from "react-slick";
import Basic from './shadertoy/Basic'
import deviceDetect from "device-detect";

let linkType;
const { device } = deviceDetect();

if (["iPad", "iPod", "iPhone", "Blackberry"].indexOf(device) !== -1) {
    linkType = "desktop"; // "ios"
} else if (["WindowsMobile", "Android"].indexOf(device) !== -1) {
    linkType = "desktop"; // "android"
} else {
    linkType = "desktop";
}

export default class Artworks extends React.Component {

render() {
    var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
    };

    var divStyle = {
    height: "100vh",
    backgroundSize: 'cover',
    backgroundImage: `url("https://res.cloudinary.com/dahnjhzzu/image/upload/v1573483049/vs_artworks/vs_back_peyg0h.jpg")`
    };

    var history = require('browser-history')

    return (
    <div>
        <div onClick={() => {history(-1)}} className="buttonBack"><i className="fas fa-chevron-left"></i></div>
        
        <div>
        {linkType === "desktop" ? (
            <Basic /> 
        ) : (
            <div className = "divStyle" style={divStyle}></div>
        )}
        </div> 
        
        <div className="container">
        <Slider {...settings}>
            
        </Slider>
        </div>
    </div>
    );
    }
}
    