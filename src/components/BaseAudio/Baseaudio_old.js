import { useState } from 'react';
import React from 'react';
import { ReactComponent as Play } from './assets/play.svg';
import { ReactComponent as Pause } from './assets/pause.svg';
import "./base-audio.css";

let audio_var = new Audio();
let datasrc = 'https://www.dropbox.com/s/m78d83xfxut72zw/Kipol%20-%20first_state.mp3';

audio_var.src = datasrc;

const Baseaudio  =  () => {
  const [isPlaying, setPlaying] = useState(true);

  const onPlayPauseClick = () => {
    
    isPlaying ? audio_var.play() : audio_var.pause();

    setPlaying(!isPlaying)
  }

  return (
    <div className="audio-controls">
      {isPlaying ? (
        <button
          type="button"
          className="play"
          onClick={() => onPlayPauseClick()}
          aria-label="Play"
        >
          <Play />
        </button>
      ) : (
        <button
          type="button"
          className="pause"
          onClick={() => onPlayPauseClick()}
          aria-label="Pause"
        >
          <Pause />
        </button>
      )}
    </div>
  );
}

export default Baseaudio
